# LED - Exercice 2:

Maintenant que vous savez comment allumer une led de la couleur de votre choix, nous allons voir comment rendre l'éclairage moins statique.

Pour cela, nous allons utiliser le temps, il va nous servir à modéliser le changement d'état impliquant une modification d'état de la led.

Pour rajouter du temps dans votre programme, vous devez rajouter cette ligne en haut de votre code :

	import time

puis vous devrez utiliser cette fonction pour que programme se mette en pause pendant 500 ms :

	time.sleep_ms(500)

Enfin pour eteindre une led il faut utiliser cette fonction : 

	votre_led.off()

### A vous de jouer :
Vous avez maintenant toutes les clefs en main pour faire clignoter la led bleu.

## Correction Exercice 2 :

```python
#pyb est le lien vers la carte qu'on utilise
import pyb
    
print("La led bleu clignotte !!")
    
#initialisation
led_bleu = pyb.LED(1)
 
#clignottement
while(1):#boucle infinie
	led_bleu.on()
	#pause pendant 500 ms
	time.sleep_ms(500)
	led_bleu.off()
	# ne pas oublier celui la sinon vous ne verrez pas la diode eteinte.
	time.sleep_ms(500)
```

