# GPIO - Exercice 1:
**Objectif** : reconnaitre l'état d'un bouton (enclenché ou relaché)
Pour cette première fonctionnalité, le code vous sera donné afin que vous compreniez comment il fonctionne.

**Lisez le code.** Les lignes précédées de '#' sont des commentaires visant à vous aider à comprendre comment s'articule le code.

```python
#pyb est le lien vers la carte qu'on utilise
import pyb
import time
    
print("Bouton 1")
    
#initialisation du premier pin/bouton (SW1)
sw1 = pyb.Pin('SW1', pyb.Pin.IN)
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

#boucle inifinie
while(1):
	time.sleep_ms(300)#pause du programme pour récupérer les valeurs du bouton toutes les 300 ms seulement

	#récupération de l'état du bouton
	valeur_sw1 = sw1.value()

	if valeur_sw1 ==0: #si le bouton est enclenché
		print("bouton 1 enclenche")
```

Une fois que vous avez compris le code essayer d'afficher une phrase lorsque les 2 autres boutons ("SW2" et "SW3" sont enclenchés)
+ si pin 1 enclenché écrire "bouton 1"
+ si pin 2 enclenché écrire "bouton 2"
+ si pin 3 enclenché ecrire "bouton 3"


## Correction Exercice 1:

```python
#pyb est le lien vers la carte qu'on utilise
import pyb
import time
    
print("Appuyez les tous !")
    
#initialisation du premier pin/bouton (SW1)
sw1 = pyb.Pin('SW1', pyb.Pin.IN)
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

#init pin 2
sw2 = pyb.Pin('SW2', pyb.Pin.IN)
sw2.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

#init pin3
sw3 = pyb.Pin('SW3', pyb.Pin.IN)
sw3.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

#boucle inifinie
while(1):
	time.sleep_ms(300)#pause du programme pour récupérer les valeurs du bouton toutes les 300 ms seulement

	#récupération de l'état du bouton
	valeur_sw1 = sw1.value()
	valeur_sw2 = sw2.value()
	valeur_sw3 = sw3.value()


	if valeur_sw1 ==0: #si le bouton est enclenché
		print("bouton 1")

	if valeur_sw2 ==0: 
		print("bouton 2")

	if valeur_sw3 ==0: 
		print("bouton 3")
```