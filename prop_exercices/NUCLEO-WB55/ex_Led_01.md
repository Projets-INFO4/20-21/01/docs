# LED - Exercice 1:
**Objectif** : Allumer la led bleu
Pour cette première fonctionnalité, le code vous sera donné afin que vous compreniez comment il fonctionne.

**Lisez le code.** Les lignes précédées de '#' sont des commentaires visant à vous aider à comprendre comment s'articule le code.

```python
    #pyb est le lien vers la carte qu'on utilise
    import pyb
    
    print("Allez les Bleus !!")
    
    #initialisation de la variable "led_bleu" avec la led 1 de la carte
    led_bleu = pyb.LED(1)
    
    #allumage de la led 1 (bleu)
    led_bleu.on()
```

Maintenant que vous avez bien compris cela, essayez d'allumer les led vertes et rouges. 

*Indication : La led verte correspond à LED(2) et la rouge à LED(3)*

## Correction Exercice 1:
Si vous n'avez pas le même code en sortie, ce n'est pas un problème, l'important est que sur votre carte, les leds verte et rouge soit allumées.

```python
#pyb est le lien vers la carte qu'on utilise
import pyb
    
print("Allumons les Vert et les rouges !!")
    
#initialisation
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)
    
#allumage des leds
led_vert.on()
led_rouge.on()
```