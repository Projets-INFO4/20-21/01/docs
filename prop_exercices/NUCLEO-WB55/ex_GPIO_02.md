# GPIO - Exercice 2:
**Objectif** : allumer une led en fonction du bouton enclenché

L'exercice précedent vous a appris a reconnaitre l'état d'un bouton:
- afficher une phrase si le bouton est appuyé
- ne rien afficher si le bouton n'est pas appuyé

Dans les exercices sur les leds, vous savez maintenant comment allumer et eteindre une led.

Maintenant essayez d'allumer les leds en fonction de l'appuie des boutons :
+ si le bouton 1 est enclenché, la led bleue est allumée
+ si le bouton 2 est enclenché, la led verte est allumée
+ si le bouton 3 est enclenché, la led rouge est allumée

Bien entendu si les boutons ne sont pas enclenchés les leds sont éteintes.

## Correction Exercice 2:

```python
#pyb est le lien vers la carte qu'on utilise
import pyb
import time
    
print("Allumez les tous !")
    
#initialisation des pins/boutons
sw1 = pyb.Pin('SW1', pyb.Pin.IN)
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw2 = pyb.Pin('SW2', pyb.Pin.IN)
sw2.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw3 = pyb.Pin('SW3', pyb.Pin.IN)
sw3.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

#initialisation des led
led_bleu = pyb.LED(1)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)

#boucle inifinie
while(1):
	time.sleep_ms(300)#pause du programme pour récupérer les valeurs du bouton toutes les 300 ms seulement

	#récupération de l'état du bouton
	valeur_sw1 = sw1.value()
	valeur_sw2 = sw2.value()
	valeur_sw3 = sw3.value()

	if valeur_sw1 ==0: #si le bouton 1 est enclenché
		led_bleu.on()
	else:#si le bouton 1 est relaché
		led_bleu.off()

	if valeur_sw2 ==0:
		led_vert.on()
	else:
		led_vert.off()

	if valeur_sw3 ==0: 
		led_rouge.on()
	else:
		led_rouge.off()
```