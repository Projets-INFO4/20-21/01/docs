# Introduction

## Project STM32Python
A new academic reform asks the high school teachers to teach a new discipline to the students : the Digital and Technological Science.
A topic seen is this domain is the Internet of Things (IoT) covered by the chapter "embedded and connected objects" wich represent an extension of the internet to the physic world.
The goal is to bring the students to a first level of IoT's understanding.
The stakes is to promote a chosen orientation, here, the numeric engineery. The academic reform increased the share of "numeric" and "digital" in the education.

## Students

Bertrand Baudeur (team leader)<br/>
Émilie Tondeux<br/>
Alexis Lanquetin<br/>

## Links

Follow up sheet : https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/01/docs/-/blob/master/Contribution%20au%20projet%20STM32Python%20info4_2020_2021.md <br/>
Projet git repository : https://gitlab.com/stm32python/fr-v2 <br/>

## Teachers

Didier DONSEZ


