# Follow-up sheet

## Week of 18/01/2021

Project attribution.

## Week of 25/01/2021

Getting missing supplies at FabLab.<br/>
Following the course about LoRa and Riot by Didier DONSEZ.

## Week of 1/02/2021

Discussion with Didier DONSEZ about how STM32Python project works.

## Week of 8/02/2021

### Completed Tasks
Trying to install the environnement to make the board works on Linux. Did not succeed.<br/>
Installing the environnement to work with the board on windows. Succeed.<br/>
<br/>
Thinking about the webside, it's presentation, clearness and flaws.<br/>
    -Some Tutorials are not clear (More details to come).<br/>
    -There's a lot of tutorials all around, it's hard to know wich one to follow.<br/>
    -All download links leads to the same page where you have to find again what you want.<br/>
    -No real exercice, all "exercices" are tutorials on how to use the board. There's no practice.<br/>


### Goals for the next session (in two weeks)

Make an inventory of all the elements we want to improve on the website.<br/>
Create a Trello board for the team.<br/>
Contact Michael Escoda about the following subjects :<br/>
    -Inventory of website propositions.<br/>
    -Ask him what we can do with the usb STM32WB55.<br/>
    -Organize a meeting.<br/>
    -STM32 Slack.<br/>
    -Linux installation issues.<br/>

<br/>
Familiarize with Jekyll<br/>
Familiarize with MakeCode<br/>
Build a list of all available ressources.<br/>

## Week of 22/02/2021

### Advancements since last time :
Familiarization with Jekyll advanced a lot, but we encounter a problem with the gemfile.<br/>
Checking what makecode is.<br/>
Checking what app inventor is.<br/>
Dressing up a list of elements that we want to change.<br/>
Sending a mail to Michael Escoda.<br/>
Creating a Trello.<br/>

### Goal for the next session :
Master Jekyll to be able to modify the website.<br/>
Create a list of simple exercices to get started.<br/>
Make tutorials more precise.<br/>

## Week of 01/03/2021

### Advancements since last week :
Reviewing some of the tutorials availaible one the site.<br/>
Discuss with Michael Escoda and Yannick Marietti.<br/>
Out of this discussion we know what we will do.<br/>
Creating of a new git repository to build a new themed website.<br/>
Planning the appearance of the site.<br/>

### Goal for the next week :
Starting the building of the new website (make it easy to visualize locally).<br/>
Making documentation of how it will look and be organized present it to Michael Escoda for approval.<br/>
Make a consistent planning.<br/>

## Week of 08/03/2021

### Advancements since last week :
Using TeXt Theme with Jekyll and start building landing page skeleton.<br/>
organizing some of the files from TeXt Theme.<br/>
Finish the navigation skeleton of the website.<br/>
Presenting the site to Michael Escoda for visual and structure approval.<br/>

### Goal for the next week :
Enhance website visual aspect. Continue adding categories, images.<br/>
Bringing some tutorials from the old website to the new one.<br/>

## Week of 15/03/2021

### Advancements since last week :
Michael Escoda gave his approval for the current structure of the website.<br/>
Website almost completely skinned.<br/>
Header and footer are completed.<br/>

### Goal for the next week :
Ask Michael Escoda for images from ST and graphical creations.<br/>
Add even more tutorials to the website and review them.<br/>
Make the website work on the internet (not local) version. <br/>

## Week of 22/03/2021

### Advancements since last week :
Finish lots of small elements to the website, adding visual changes. <br/>
Discuss with Michael Escoda about the "done" version of the Website. <br/>
Michael Escoda will give us a list of elements to change for this week. <br/>

### Goal for the next week :
Present to Michael Escoda the site with the modification he asked for. <br/>
Create tutorials with MIT App Inventor. <br/>
Change the tutorial about docker to make it up to date. <br/>
Create documentation on how to use the new website. <br/>

## Week of 29/03/2021

### Advancements since last week :
Discussed with Michael Escoda about the done website. He now wants us to implements the strucutre to support different languages <br/>
We started to make a tutorial with MIT AppInventor but failed to make the card communicate with the app. <br/>

### Goal for the next week (project end):
Finish the support of multiple languages and deliver the site to Michael Escoda. <br/>
Try to make MIT AppInventor work. <br/>

## Project end :

### What has been done since last time :


### What next ?
